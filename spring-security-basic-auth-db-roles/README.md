Spring Security

1) Spring security is based on filters.
2) We can have as many filters needed before the request reaches the servlet.
3) Filter gives request to "Authentication Manager" -> "Authentication Provider"
4) For basic auth we need to implement "UserDetailsService" and a "Password Encoder"
5) In this example just implementing UserDetails service is good enough, because of spring boot basic auth is enabled by default.
   * This bean is automatically picked up by spring boot

```User``` extends ```UserDetails```
* To implement a bunch is methods need to me overridden like 
  * ```isAccountLocked -> boolean```
  * ```getUsername -> String```
  *  ```getAuthorities() -> List<SecurityAuthority>```
    * ```SecurityAuthority``` is another contract which primarily deals with authorization
    *  When we get the user object from database we can also this implement this method to roles.


DB Schema

Users Table
```users```

| id [primary key] |  username   |       password |
|:-----------------|:-----------:|---------------:|
| number           | varchar(25) |    varchar(25) |
| 1                |    john     |       password |
| 2                |     jan     | SecurePassword |

Authority Table
```authorities```

| id [primary key] |    name     | users [foreign key] |
|:-----------------|:-----------:|--------------------:|
| number           | varchar(25) |              number |
| 1                |    READ     |                   2 |
| 2                |    WRITE    |                   1 |

user-authorities-mapping
```users_authorities```

| user_id [foreign key] | authority_id [foreign key] |
|:----------------------|:--------------------------:|
| Integer               |          Integer           |
| 1                     |             2              |
| 1                     |             1              |


```mermaid
flowchart TD
        A[Consumer with basic auth token] -->
        B[JpaUserDetailsService]  -->
        C[Jpa get users and associated roles] -->
        D[SecurityUser Object is created with username and roles] -->
        E[SecurityContext is set by spring] -->
        F[Controller get information from security Context]


```