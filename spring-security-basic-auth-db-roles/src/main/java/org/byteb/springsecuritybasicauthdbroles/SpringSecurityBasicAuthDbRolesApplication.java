package org.byteb.springsecuritybasicauthdbroles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityBasicAuthDbRolesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityBasicAuthDbRolesApplication.class, args);
    }

}
