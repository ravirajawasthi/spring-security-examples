package org.byteb.springsecuritybasicauthdbroles.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class HelloController {

    @GetMapping("/")
    public Response demo() {
        var authenticatedUser = SecurityContextHolder.getContext().getAuthentication();
        var response = new Response("Hii " + authenticatedUser.getName() + "! You are authorized!");
        var respRoleList = response.getAllowedRoles();
        authenticatedUser.getAuthorities().forEach(grantedAuthority -> respRoleList.add(grantedAuthority.getAuthority()));
        return response;
    }

}
