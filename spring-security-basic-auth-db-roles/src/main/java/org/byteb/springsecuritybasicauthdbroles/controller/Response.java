package org.byteb.springsecuritybasicauthdbroles.controller;


import java.util.ArrayList;
import java.util.List;

public class Response {

    private String message;
    private List<String> allowedRoles;

    public Response(String message) {
        this.message = message;
        this.allowedRoles = new ArrayList<>();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getAllowedRoles() {
        return allowedRoles;
    }

    public void setAllowedRoles(List<String> allowedRoles) {
        this.allowedRoles = allowedRoles;
    }
}
