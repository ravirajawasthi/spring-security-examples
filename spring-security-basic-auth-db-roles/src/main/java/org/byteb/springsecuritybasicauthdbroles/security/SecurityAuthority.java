package org.byteb.springsecuritybasicauthdbroles.security;

import lombok.AllArgsConstructor;
import org.byteb.springsecuritybasicauthdbroles.entities.Authority;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
public class SecurityAuthority implements GrantedAuthority {

    private final Authority string;

    @Override
    public String getAuthority() {
        return string.getName();
    }

}
