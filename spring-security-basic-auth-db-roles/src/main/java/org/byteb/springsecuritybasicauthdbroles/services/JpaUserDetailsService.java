package org.byteb.springsecuritybasicauthdbroles.services;

import lombok.AllArgsConstructor;
import org.byteb.springsecuritybasicauthdbroles.repositories.UserRepository;
import org.byteb.springsecuritybasicauthdbroles.security.SecurityUser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class JpaUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        var u = userRepository.findUserByUsername(username);
        return u.map(SecurityUser::new).orElseThrow(() -> new UsernameNotFoundException("Username not found : " + username));
    }
}
