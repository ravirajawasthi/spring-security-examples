--Only 1 TIME
create database c2;
create schema ss2;
alter schema ss2 owner to manager;

create table ss2.users
(
    id       integer primary key,
    username varchar(30) not null,
    password varchar(50) not null
);

insert into ss2.users
values (1, 'john', 'password');
insert into ss2.users
values (2, 'jan', 'securePassword');


create table ss2.authorities
(
    id    integer primary key,
    roles varchar(20) not null
);

insert into ss2.authorities
values (1, 'READ');
insert into ss2.authorities
values (2, 'WRITE');

create table ss2.users_authorities
(
    user_id      integer references ss2.users (id),
    authority_id integer references ss2.authorities (id)
);

insert into ss2.users_authorities
values (1, 1);
insert into ss2.users_authorities
values (1, 2);
insert into ss2.users_authorities
values (2, 1);


--RESET
drop table ss2.authorities;
drop table ss2.users;
drop table ss2.users_authorities;