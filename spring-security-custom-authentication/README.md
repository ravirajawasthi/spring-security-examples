### Consumer sends A API key that is checked in headers

```
{
    "key": "KEY"
}
```

Basic Components 
1) ```CustomAuthenticationFilter``` is defined
2) A _unauthenticated_ object of ```CustomAuthentication``` is created
3) This object is passed to ```CustomeAuthenticationManager```. A instance of ```Authentication``` is returned
4) ```CustomAuthenticationManager``` delegates this job to a instance of ```CustomAuthenticationProvider```.
   * Before delegating it also check if it supports this type of authentication. 
     * The ```CustomAuthentication``` we created, its purpose it to carry the token consumer sent. Our Provider will implement 2 methods, ```authenticate``` and ```supports```. ```supports``` helps us here.
5) The ```CustomAuthenticationProvider``` will then validate the token. It will return a new object of ```CustomerAuthentication``` with ```authenticated``` as ```true```.
6) This same is also set in security context using ```SecurityContextHolder.getContext().setAuthentication(a)```.

*** In this case we throw exception if the client isn't using authentication that we support. Which is quite incorrect

### Spring security components

1) ````OncePerRequestFilter``` 
provides :```doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)```

2) ```AuthenticationManager```
provides : ```Authentication authenticate(Authentication authentication)```
If we support the authentication provided then we can deal with it or skip our filter and pass it ahead

3) ```AuthenticationProvider```
provides: ```supports``` and ```authenticate```

4) ```Authentication```
Field related to our authentication are added here