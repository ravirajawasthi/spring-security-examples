package org.byteb.spring.security.custom.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityCustomAuthenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityCustomAuthenticationApplication.class, args);
    }

}
