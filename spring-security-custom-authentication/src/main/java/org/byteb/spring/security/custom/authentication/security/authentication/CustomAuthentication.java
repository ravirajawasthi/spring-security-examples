package org.byteb.spring.security.custom.authentication.security.authentication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import javax.security.auth.Subject;
import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
public class CustomAuthentication implements Authentication {


    /*
    lot of unnecessary methods that need to implement
    TODO
    Could use record here. but some of theses classes can be subject to "aspect" by spring. need to check that out
     */
    private final boolean authentication;
    //if this field changes then need create a new object
    private final String key;

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }


    /**
     * Returns true if the specified subject is implied by this principal.
     *
     * @param subject the {@code Subject}
     * @return true if {@code subject} is non-null and is
     * implied by this principal, or false otherwise.
     * @implSpec The default implementation of this method returns true if
     * {@code subject} is non-null and contains at least one principal that
     * is equal to this principal.
     *
     * <p>Subclasses may override this with a different implementation, if
     * necessary.
     * @since 1.8
     */
    @Override
    public boolean implies(Subject subject) {
        return Authentication.super.implies(subject);
    }

    /**
     * Returns the name of this principal.
     *
     * @return the name of this principal.
     */
    @Override
    public String getName() {
        return null;
        //No need in our implementation
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
        //No need in our implementation
    }

    @Override
    public Object getCredentials() {
        return null;
        //No need in our implementation
    }

    @Override
    public Object getDetails() {
        return null;
        //No need in our implementation
    }

    @Override
    public Object getPrincipal() {
        return null;
        //No need in our implementation
    }


}
