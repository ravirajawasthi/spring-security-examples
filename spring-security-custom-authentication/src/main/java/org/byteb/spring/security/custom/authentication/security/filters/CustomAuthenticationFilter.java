package org.byteb.spring.security.custom.authentication.security.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.byteb.spring.security.custom.authentication.security.authentication.CustomAuthentication;
import org.byteb.spring.security.custom.authentication.security.managers.CustomAuthenticationManager;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Configuration
@AllArgsConstructor
public class CustomAuthenticationFilter extends OncePerRequestFilter {


    //There is no guarantee filter will be called once

    private final CustomAuthenticationManager customAuthenticationManager;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        //Create an authentication object which is not yet authenticated
        String key = String.valueOf(request.getHeader("key"));
        CustomAuthentication ca = new CustomAuthentication(false, key);

        //Delegate the authentication object to the manager
        Authentication a = customAuthenticationManager.authenticate(ca);

        //if the object is authenticated then send request to next filter in the chain.
        if (a.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(a);
            //Authorization is still pending. so authorization mechanism can still check if we are Authorized
            filterChain.doFilter(request, response); //Only when authentication works
        }


    }
}
