package org.byteb.spring.security.custom.authentication.security.providers;

import org.byteb.spring.security.custom.authentication.security.authentication.CustomAuthentication;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Value("${authentication.security.secret.key}")
    private String secretKey;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        CustomAuthentication ca = (CustomAuthentication) authentication;

        String headerKey = ca.getKey();
        //This is where actual authentication happens
        if (headerKey.equals(secretKey)) {
            //Returning new object because both fields are final.
            //Also removing key because its no longer required
            return new CustomAuthentication(true, null);
        }

        throw new BadCredentialsException("Provided key is invalid");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        //This will determine if this provider supports some authentication
        return CustomAuthentication.class.equals(authentication);
    }
}
