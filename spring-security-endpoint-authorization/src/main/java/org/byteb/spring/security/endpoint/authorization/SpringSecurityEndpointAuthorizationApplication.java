package org.byteb.spring.security.endpoint.authorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityEndpointAuthorizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityEndpointAuthorizationApplication.class, args);
    }

}
