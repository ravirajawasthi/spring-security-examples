package org.byteb.spring.security.endpoint.authorization.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity.httpBasic()
                .and()
                .authorizeHttpRequests()
                //                .anyRequest().hasRole("ADMIN")

                .anyRequest().hasAnyAuthority("read", "write")
                //                        .anyRequest().authenticated() //authenticated here is very important
                //                        .anyRequest().permitAll()
                //                .anyRequest(). //SPeL -> spring language for checking
                // authority
                .and()
                .build();
    }

    //Role - it represents group of action a person can perform (prefixed by ROLE_) by spring security
    //        .roles("ROLE_ADMIN") it is not allowed

    //Authority - it represents a permission to do 1 action
    //Both role and authority implement GrantedAuthority

    //If we have to check for both roles and authority then we can do like .hasAnyAuthority("read", "ROLE_ADMIN")


    //     1) Which methods to use to apply authentication to different endpoints (anyRequest(), antMatcher(), mvcMatcher
    //     (), regexMatcher() )
    //            should always use mvcMatcher
    //     2) How to apply authorization rules differently ( hasAuthority(), hasAnyAuthority(), hasRole() ...)


    @Bean
    public UserDetailsService userDetailsService() {

        var uds = new InMemoryUserDetailsManager();

        var ud =
                User.withUsername("bob").password(getBcryptPasswordEncoder().encode("123456"))
                        .authorities("read")
                        //                        .roles("ADMIN")
                        .build();

        var ud2 =
                User.withUsername("john").password(getBcryptPasswordEncoder().encode("123456"))
                        //                        .roles("MANAGER")
                        .authorities("write")
                        .build();
        uds.createUser(ud);
        uds.createUser(ud2);

        return uds;
    }

    @Bean
    public PasswordEncoder getBcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
