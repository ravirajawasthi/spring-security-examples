package org.byteb.spring.security.endpoint.authorization.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/armadillo")
    public String helloWorld() {
        return "Hello you are authorized!";
    }

    @GetMapping("/valentine")
    public String helloValentine() {
        return "Hello! Welcome to Valentine!";
    }

    @GetMapping("/blackwater")
    public String welcomeBlackwater() {
        return "Hello! Welcome to BlackWater! Only Admins can come here.";
    }

}
