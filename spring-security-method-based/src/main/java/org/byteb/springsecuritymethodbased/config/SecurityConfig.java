package org.byteb.springsecuritymethodbased.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@EnableMethodSecurity
@Configuration
public class SecurityConfig {

    @Bean
    SecurityFilterChain configureSecurityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity.httpBasic(auth -> {/** Keep default values **/}).authorizeHttpRequests(auth -> auth.anyRequest().authenticated()).build();
    }

    @Bean
    UserDetailsService getUserDetailsService() {
        UserDetails u1 = User.withUsername("john").password(getBCryptPasswordEncoder().encode("password")).authorities("read").build();

        UserDetails u2 = User.withUsername("bill").password(getBCryptPasswordEncoder().encode("password")).authorities("write").build();

        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(u1);
        manager.createUser(u2);

        return manager;
    }

    @Bean
    BCryptPasswordEncoder getBCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
