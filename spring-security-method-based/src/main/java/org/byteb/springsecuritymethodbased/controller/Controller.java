package org.byteb.springsecuritymethodbased.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/")
    @PreAuthorize("hasAuthority('read')")
    String hello() {
        return "You are Authorized";
    }

    @GetMapping("/hello")
    @PreAuthorize("hasAnyAuthority('read', 'write')")
    String helloBoth() {
        return "You are Authorized";
    }

}
