# Basic and Custom Auth

## Custom Auth

- A header with name `x-api-key` containing the key is required

## Basic Auth

- A header `Authorization` starting with `Basic <Token>` is required.
- By Default username is : `user` and password will be generated in the logs whenever the application is started.

## How custom auth is implemented

1. `ApiKeyFilter` ---`(ApiKeyAuthentication)`--> `CustomAuthenticationManager (extending Authentication Manager)` --> `ApiKeyProvider (extending Authentication Provider)`
2. `ApiKeyAuthentication` implements Authentication contract.

- To implement that many methods need to be implemented like isAuthenticated, getRoles etc.

3. `CustomAuthenticationManager` implements Authentication Manager

- only one method is overridden called `authenticate`. its takes a parameter of `Authentication` type which is
  implemented by `ApiKeyAuthentication`

4. `ApiKeyProvider` extends `AuthenticationProvider`
    - The real authentication happens here.
    - This mainly overrides 2 methods `authenticate` and `supports`
    - Authentication Manager can delegate the task to any Authentication Provider, that's why we implement support.

5. `ApiKeyFilter` this implements OncePerRequestFilter (Right now I have no clue about other types of filter)
    - This is where we auth fails then we can return 401
    - If we don't support the auth provided in required then we can pass it along the filter.

## Notes

1. We check in `ApiKeyFilter` if we support this auth and again check in `ApiKeyProvider`
    - Suppose a use case where we have different types of api keys all coming under the header `x-api-key`
    - This way we can check in filter if we have the key, and based on the key we can check which provider system is
      suitable for the key we have
