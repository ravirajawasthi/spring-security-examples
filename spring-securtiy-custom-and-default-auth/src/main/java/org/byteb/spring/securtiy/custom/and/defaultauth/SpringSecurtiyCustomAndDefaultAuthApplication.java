package org.byteb.spring.securtiy.custom.and.defaultauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurtiyCustomAndDefaultAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurtiyCustomAndDefaultAuthApplication.class, args);
	}

}
