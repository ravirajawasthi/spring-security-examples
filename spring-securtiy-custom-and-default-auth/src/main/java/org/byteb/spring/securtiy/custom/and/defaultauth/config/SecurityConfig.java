package org.byteb.spring.securtiy.custom.and.defaultauth.config;

import org.byteb.spring.securtiy.custom.and.defaultauth.filters.ApiKeyFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
public class SecurityConfig {
    @Value("${api.secret.key}")
    private String key;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpFilter) throws Exception {
        return httpFilter.httpBasic()
                .and().addFilterBefore(new ApiKeyFilter(key), BasicAuthenticationFilter.class).
                authorizeHttpRequests().anyRequest().authenticated().and().
                //                .and().authenticationManager(). or by adding a bean of type AuthenticationManager
                //                 and().authenticationProvider() doesn't override AP, adds another AP
                        build();
    }

}
