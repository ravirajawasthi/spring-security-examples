package org.byteb.spring.securtiy.custom.and.defaultauth.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecuredController {

    @GetMapping("/")
    public String helloWorld() {
        return "You are authenticated!";
    }

}
